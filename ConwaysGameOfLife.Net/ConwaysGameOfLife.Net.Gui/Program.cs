﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConwaysGameOfLife.Net.Extensions;
using System.Threading.Tasks;
using System.Threading;

namespace ConwaysGameOfLife.Net.Gui
{
	public static class Program
	{
		static void Main(string[] args)
		{
			int rows = 20;
			int cols = 60;

			GameFields.GameField2D gameField2D = new GameFields.GameField2D(rows, cols);

			Rulesets.Standard2DRuleset ruleset = new Rulesets.Standard2DRuleset();
			gameField2D.Rules = ruleset;

			gameField2D.InitializeField("C:\\ProgrammTestLogs\\test.cgf");

			int x = 0;

			do
			{
				Console.Clear();
				for (int row = 0; row < rows; row++)
				{
					for (int col = 0; col < cols; col++)
					{
						char c = gameField2D.Cells[row, col].Cellstate.GetCharFromCellstate('.','o','.');
						//switch (c)
						//{
						//	case '.':
						//		Console.ForegroundColor = ConsoleColor.Gray;
						//		break;
						//	case 'o':
						//		Console.ForegroundColor = ConsoleColor.Green;
						//		break;
						//	case 'x':
						//		Console.ForegroundColor = ConsoleColor.Cyan;
						//		break;
						//}

						Console.Write(c);
					}
					Console.WriteLine();
				}

				gameField2D.NextRound();
				Thread.Sleep(500);

				if(x++ % 25 == 0)
				{
					gameField2D.RandomaizeField(100);
				}
			} while (!Console.KeyAvailable);
		}
	}
}
