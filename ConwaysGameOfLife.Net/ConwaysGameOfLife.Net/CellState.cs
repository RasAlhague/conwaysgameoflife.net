﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConwaysGameOfLife.Net
{
    public enum CellState
    {
        Dead,
        Alive,
        HasLived
    }
}
