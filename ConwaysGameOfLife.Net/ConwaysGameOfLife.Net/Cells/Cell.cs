﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConwaysGameOfLife.Net.Cells
{
    public class Cell : CellBase
    {
        public Cell() : this(new List<CellBase>())
        {
        }

        public Cell(IEnumerable<CellBase> neighbours) : base(neighbours)
        {
        }
    }
}
