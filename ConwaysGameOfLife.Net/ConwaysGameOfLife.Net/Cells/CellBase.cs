﻿using ConwaysGameOfLife.Net.Rulesets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConwaysGameOfLife.Net.Cells
{
    public abstract class CellBase
    {
        protected CellState _cellstate;
        protected CellState _tempstate;
        protected List<CellBase> _neighbours;

        protected CellBase() : this(new List<CellBase>())
        {

        }

        protected CellBase(IEnumerable<CellBase> neighbours)
        {
            _cellstate = CellState.Dead;
            _tempstate = Cellstate;
            _neighbours = neighbours.ToList();
        }

        public IRuleset Rules { get; set; }
        public virtual CellState Cellstate
        {
            get
            {
                return _cellstate;
            }
            set
            {
                _cellstate = value;
            }
        }
        public List<CellBase> Neighbours
        {
            get
            {
                return _neighbours;
            }
        }

        public virtual void SetTempState()
        {
            _tempstate = Rules.CheckRules(Neighbours, this.Cellstate);
        }

        public virtual void SetNewState()
        {
            Cellstate = _tempstate;
        }
    }
}
