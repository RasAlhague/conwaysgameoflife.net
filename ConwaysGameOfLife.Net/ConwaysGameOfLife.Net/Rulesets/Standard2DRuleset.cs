﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConwaysGameOfLife.Net.Cells;

namespace ConwaysGameOfLife.Net.Rulesets
{
	public class Standard2DRuleset : Ruleset
	{
		public override CellState CheckRules(IEnumerable<CellBase> neighbours, CellState currentState)
		{
			var livingNeighbours = neighbours.Count(x => x.Cellstate == CellState.Alive);

			if(currentState == CellState.Dead && livingNeighbours == 3)
			{
				return CellState.Alive;
			}
			else if (currentState == CellState.Alive && livingNeighbours < 2)
			{
				return CellState.HasLived;
			}
			else if (currentState == CellState.Alive &&  livingNeighbours > 3)
			{
				return CellState.HasLived;
			}
			else
			{
				return currentState;
			}
		}
	}
}
