﻿using ConwaysGameOfLife.Net.Cells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConwaysGameOfLife.Net.Rulesets
{
    public abstract class Ruleset : IRuleset
    {
        public abstract CellState CheckRules(IEnumerable<Cells.CellBase> neighbours, CellState currentState);
    }
}
