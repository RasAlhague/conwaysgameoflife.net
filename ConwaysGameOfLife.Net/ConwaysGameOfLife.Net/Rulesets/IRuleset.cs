﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConwaysGameOfLife.Net.Rulesets
{
    public interface IRuleset
    {
        CellState CheckRules(IEnumerable<Cells.CellBase> neighbours, CellState currentState);
    }
}
