﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConwaysGameOfLife.Net.GameFields
{
    public abstract class GameField
    {
        public Rulesets.IRuleset Rules { get; set; }

        public abstract void InitializeField(string source, char deadSign = '.', char aliveSign = 'o', char hasLivedSign = 'x');
        public abstract void NextRound();
        public abstract void ExportField(string dest);
        public abstract void RandomaizeField(int countOfLivingCells);
    }
}
