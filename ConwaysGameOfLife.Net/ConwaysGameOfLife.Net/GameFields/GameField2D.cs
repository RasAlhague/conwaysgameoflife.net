﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ConwaysGameOfLife.Net.Extensions;

namespace ConwaysGameOfLife.Net.GameFields
{
	public class GameField2D : GameField
	{
		public Cells.CellBase[,] Cells { get; private set; }
		private long _rows;
		private long _columns;

		public GameField2D(int rows, int columns) : this((long)rows, (long)columns)
		{
		}

		public GameField2D(long rows, long columns)
		{
			_rows = rows;
			_columns = columns;
			Cells = new Cells.CellBase[_rows, _columns];
		}

		public override void InitializeField(string source, char deadSign = '.', char aliveSign = 'o', char hasLivedSign = 'x')
		{
			string[] sourceArr = File.ReadAllLines(source);

			for (long row = 0; row < _rows; row++)
			{
				for (int column = 0; column < _columns; column++)
				{
					Cells[row, column] = new Cells.Cell();
				}
			}

			for (long row = 0; row < _rows; row++)
			{
				for (int column = 0; column < _columns; column++)
				{
					Cells.Cell cell = this.Cells[row, column] as Cells.Cell;

					if (row < sourceArr.Length)
					{
						if (column < sourceArr[row].Length)
						{
							cell.Cellstate = sourceArr[row][column].GetCellstateFromChar(deadSign, aliveSign, hasLivedSign);
						}
						else
						{
							cell.Cellstate = CellState.Dead;
						}
					}
					else
					{
						cell.Cellstate = CellState.Dead;
					}
					cell.Rules = this.Rules;

					CheckIndexForNeigbourCol(row - 1, column - 1, cell);
					CheckIndexForNeigbourCol(row - 1, column, cell);
					CheckIndexForNeigbourCol(row - 1, column + 1, cell);
					CheckIndexForNeigbourCol(row, column - 1, cell);
					CheckIndexForNeigbourCol(row, column + 1, cell);
					CheckIndexForNeigbourCol(row + 1, column - 1, cell);
					CheckIndexForNeigbourCol(row + 1, column, cell);
					CheckIndexForNeigbourCol(row + 1, column + 1, cell);
				}
			}
		}

		private void CheckIndexForNeigbourCol(long row, int column, Cells.Cell cell)
		{
			if (row >= 0 && row < _rows && column >= 0 && column < _columns)
			{
				cell.Neighbours.Add(Cells[row, column]);
			}
		}

		public override void NextRound()
		{
			foreach (var cell in Cells)
			{
				cell.SetTempState();
			}

			foreach (var cell in Cells)
			{
				cell.SetNewState();
			}
		}

		public override void ExportField(string dest)
		{
			FileStream fs = new FileStream(Path.Combine(dest, ".cgf"), FileMode.Create);

			using (StreamWriter sw = new StreamWriter(fs))
			{
				for (int row = 0; row < _rows; row++)
				{
					for (int column = 0; column < _columns; column++)
					{
						sw.Write(Cells[row, column]);
					}
					sw.WriteLine();
				}
			}
		}

		public override void RandomaizeField(int countOfLivingCells)
		{
			for (int i = 0; i < countOfLivingCells; i++)
			{
				Random random = new Random();

				int row = random.Next(0, Convert.ToInt32(_rows - 1));
				int column = random.Next(0, Convert.ToInt32(_columns - 1));

				Thread.Sleep(2);

				Cells[row, column].Cellstate = CellState.Alive;
			}
		}
	}
}
