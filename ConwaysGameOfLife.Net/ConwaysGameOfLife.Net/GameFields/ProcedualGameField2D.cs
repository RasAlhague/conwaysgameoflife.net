﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConwaysGameOfLife.Net.GameFields
{
    public class ProcedualGameField2D : GameField
    {
        public char[,] Cells { get; set; }

        public ProcedualGameField2D(int rows, int columns)
        {
            Cells = new char[rows, columns];
        }

        public ProcedualGameField2D(long rows, long columns)
        {
            Cells = new char[rows, columns];
        }

        public override void InitializeField(string source, char deadSign = '.', char aliveSign = 'o', char hasLivedSign = 'x')
        {
            throw new NotImplementedException();
        }

        public override void NextRound()
        {
            throw new NotImplementedException();
        }

        public override void ExportField(string dest)
        {
            throw new NotImplementedException();
        }

        public override void RandomaizeField(int countOfLivingCells)
        {
            throw new NotImplementedException();
        }
    }
}
