﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConwaysGameOfLife.Net.GameFields
{
    public class GameField3D : GameField
    {
        public Cells.CellBase[, ,] Cells { get; private set; }

        public GameField3D(int tables, int rows, int columns)
        {
            Cells = new Cells.CellBase[tables, rows, columns];
        }

        public GameField3D(long tables, long rows, long columns)
        {
            Cells = new Cells.CellBase[tables, rows, columns];
        }

        public override void InitializeField(string source, char deadSign = '.', char aliveSign = 'o', char hasLivedSign = 'x')
        {
            throw new NotImplementedException();
        }

        public override void NextRound()
        {
            throw new NotImplementedException();
        }

        public override void ExportField(string dest)
        {
            throw new NotImplementedException();
        }

        public override void RandomaizeField(int countOfLivingCells)
        {
            throw new NotImplementedException();
        }
    }
}
