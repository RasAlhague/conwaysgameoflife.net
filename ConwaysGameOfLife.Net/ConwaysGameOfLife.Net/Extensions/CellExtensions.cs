﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConwaysGameOfLife.Net.Extensions
{
    public static class CellExtensions
    {
        public static CellState GetCellstateFromChar(this char sign, char dead = '.', char alive = 'o', char hasLived = 'x')
        {
            if(sign.Equals(dead))
            {
                return CellState.Dead;
            }
            else if(sign.Equals(alive))
            {
                return CellState.Alive;
            }
            else if(sign.Equals(hasLived))
            {
                return CellState.HasLived;
            }
            else
            {
                throw new ArgumentException("The given sign isn't supported!");
            }
        }

		public static char GetCharFromCellstate(this CellState state, char dead = '.', char alive = 'o', char hasLived = 'x')
		{
			if (state == CellState.Dead)
			{
				return dead;
			}
			else if (state == CellState.Alive)
			{
				return alive;
			}
			else if (state == CellState.HasLived)
			{
				return hasLived;
			}
			else
			{
				throw new ArgumentException("The given sign isn't supported!");
			}
		}
    }
}
